﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMainController : MonoBehaviour 
{
	GameObject IncomingController;

	void Start () 
	{
		IncomingController = GameObject.FindGameObjectWithTag ("IncomingController");
		if (null == IncomingController) 
		{
			Debug.Log ("NULL IncomingController");
			IncomingController = Instantiate (Resources.Load ("Prefabs/IncomingController")) as GameObject;
		}
		IncomingController.GetComponent<IncomingObjectController> ().gameStarted = true;
	}
}
