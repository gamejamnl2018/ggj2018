﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverController : MonoBehaviour 
{
	GameObject IncomingController;
	void Start () 
	{
		IncomingController = GameObject.FindGameObjectWithTag ("IncomingController");
		if (null == IncomingController) 
		{
			IncomingController = Instantiate (Resources.Load ("Prefabs/IncomingController")) as GameObject;
		}
		if (null != IncomingController) 
		{
			IncomingController.GetComponent<IncomingObjectController> ().Reset ();
		}
	}
}
