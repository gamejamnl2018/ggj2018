﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AboutController : MonoBehaviour 
{
	GameObject IncomingController;

	void Start () 
	{
		IncomingController = GameObject.FindGameObjectWithTag ("IncomingController");
		if (null == IncomingController) 
		{
			IncomingController = Instantiate (Resources.Load ("Prefabs/IncomingController")) as GameObject;
		}
	}
}
