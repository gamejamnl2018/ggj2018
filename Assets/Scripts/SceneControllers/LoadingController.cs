﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingController : MonoBehaviour 
{
	GameObject IncomingController;
	Text messageTwo;
	Text messageThree;
	float time = 0f;
	void Start () 
	{
		FindAndUpdateUIElements ();
		IncomingController = GameObject.FindGameObjectWithTag ("IncomingController");
		if (null == IncomingController) 
		{
			IncomingController = Instantiate (Resources.Load ("Prefabs/IncomingController")) as GameObject;
		}
	}

	void Update () 
	{
		time += Time.deltaTime;
		if (time > 2) 
		{
			messageTwo.enabled = true;
		}
		if (time > 4) 
		{
			messageThree.enabled = true;
		}
		if (time > 6) 
		{
			SceneManager.LoadSceneAsync ("Menu");
		}
	}

	void FindAndUpdateUIElements ()
	{
		messageTwo = GameObject.Find ("Canvas/MessageTwo").GetComponent<Text> ();
		messageTwo.enabled = false;
		messageThree = GameObject.Find ("Canvas/MessageThree").GetComponent<Text> ();
		messageThree.enabled = false;
	}
}
