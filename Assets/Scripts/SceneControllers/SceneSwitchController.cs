﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitchController : MonoBehaviour 
{
	public void SwitchScenes (string nextScene) 
	{
		SceneManager.LoadSceneAsync (nextScene);
	}
}
