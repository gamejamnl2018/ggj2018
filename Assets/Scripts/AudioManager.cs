﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {
	public static AudioManager instance = null; 
	[@SerializeField]
	AudioSource soundFx; 
	[@SerializeField]
	AudioSource gameMusic;

	


	 void Awake ()
        {
            //Check if there is already an instance of SoundManager
            if (instance == null)
                //if not, set it to this.
                instance = this;
            //If instance already exists:
            else if (instance != this)
                //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
                Destroy (gameObject);
            
            //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
            DontDestroyOnLoad (gameObject);
        }

	public void PlaySoundFx(AudioClip audioClip) { 
		soundFx.clip = audioClip; 
		soundFx.Play(); 
	}
	
	
}
