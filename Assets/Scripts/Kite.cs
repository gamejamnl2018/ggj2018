﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Kite : MonoBehaviour 
{
	Vector2 velocity;
	Rigidbody2D rigidbody;
	Rigidbody2D endRopeBody; 
	Text scoreText;
	Text angryText;
	Text gameOverText;
	public int score = 0;
	public int previousScore = 0;
	string stringScore = "Score: ";
	public bool gameOver = false;
	public float gameOverCountDown = 0f;
	public float gameOverDuration = 1f;
	Animator animator;
	GameObject player;
	GameObject ceiling;
	GameObject endRope;

	public AudioClip radioWaveHit; 
	public AudioClip bridHit; 

	void Start () 
	{
		velocity = Vector2.zero;
		rigidbody = GetComponent<Rigidbody2D> ();
		gameOverCountDown = 0f;

		FindAndUpdateUIElements ();
		FindAndUpdateOtherGameObjects ();
	}

	void Update () 
	{
		if (!gameOver) 
		{
			MoveKite ();
			UpdateScoreUI ();
		} 
		else 
		{
			GameOverSequence ();
		}
	}

	void MoveKite()
	{
		if (Input.anyKey || Input.touchCount > 0) 
		{
			velocity.y = -3f;
			rigidbody.velocity = velocity;
		} 
		else if (!Input.anyKey || Input.touchCount == 0) 
		{
			velocity.y = 3f;
			rigidbody.velocity = velocity;
		}
	}

	void GameOverSequence()
	{
		velocity.y = 3f;
		rigidbody.velocity = velocity;
		ceiling.SetActive (false);
		angryText.enabled = true;
		gameOverText.enabled = true;
		gameOverCountDown += Time.deltaTime;
		animator.SetTrigger ("gameOver");
		if (gameOverCountDown > gameOverDuration) 
		{
			SceneManager.LoadSceneAsync ("GameOver");
		}
	}

	void UpdateScoreUI()
	{
		if (score != previousScore) 
		{
			scoreText.text = stringScore + score;
			previousScore = score;
		}
	}

	void FindAndUpdateUIElements()
	{
		scoreText = GameObject.Find ("Canvas/Score").GetComponent<Text> ();
		angryText = GameObject.Find ("Canvas/Angry").GetComponent<Text> ();
		gameOverText = GameObject.Find ("Canvas/GameOverText").GetComponent<Text> ();
		scoreText.text = stringScore + score;
		angryText.enabled = false;
		gameOverText.enabled = false;
	}

	void FindAndUpdateOtherGameObjects()
	{
		player = GameObject.FindGameObjectWithTag ("Player");
		animator = player.GetComponent<Animator> ();

		ceiling = GameObject.FindGameObjectWithTag ("Ceiling");
		ceiling.SetActive (true);

		endRope = GameObject.FindGameObjectWithTag("EndRope"); 
		endRopeBody = endRope.GetComponent<Rigidbody2D>(); 
		




	}
	void OnTriggerEnter2D(Collider2D other) 
	{
		if (other.CompareTag ("Transmission") && !gameOver) 
		{
			score += 1;
			other.gameObject.SetActive (false);
			AudioManager.instance.PlaySoundFx(radioWaveHit); 
		} 
		else if (other.CompareTag ("Enemy")) 
		{
			AudioManager.instance.PlaySoundFx(bridHit); 
			endRopeBody.constraints = RigidbodyConstraints2D.None;  
			gameOver = true;
		}
	}
}
