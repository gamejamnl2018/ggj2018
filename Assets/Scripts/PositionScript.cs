﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionScript : MonoBehaviour 
{
	Rigidbody2D rigidbody;
	public Vector3 initialPosition = new Vector3(0, 0, 0);
	public Vector2 initialVelocity = new Vector2(0, 0);
	void Awake () 
	{
		rigidbody = GetComponent<Rigidbody2D> ();
		rigidbody.velocity = initialVelocity;
	}

	void OnTriggerEnter2D(Collider2D other) 
	{
		if (other.CompareTag ("Boundary")) 
		{
			Reset ();
		}
	}

	public void Reset()
	{
		this.gameObject.SetActive (false);
		this.transform.position = initialPosition;
	}

	public void OnEnable()
	{
		this.transform.position = initialPosition;
		this.rigidbody.velocity = initialVelocity;
	}
}
