﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncomingObjectController : MonoBehaviour {
	
	List<GameObject> incomingObjects;
	float[] velocityX;
	public static IncomingObjectController instance;
	static float timeDelay = 1.5f;
	static float timePassed = 0f;
	static float totalTime = 0f;
	public bool gameStarted = false;
	void Awake()
	{
		if (instance != null && instance != this) 
		{
			Destroy (this.gameObject);
		} 
		else 
		{
			instance = this;
			DontDestroyOnLoad (this.gameObject);
		}
		instance = this;
		velocityX = new float[4] {5f, 5.5f, 6f, 8f};
	}

	void Start () 
	{
		incomingObjects = new List<GameObject> ();

		GameObject bird = Instantiate(Resources.Load("Prefabs/GrayBird")) as GameObject;
		GameObject soundwave = Instantiate(Resources.Load("Prefabs/SoundWave")) as GameObject; 
		DontDestroyOnLoad(bird);
		DontDestroyOnLoad(soundwave);

		bird.SetActive(false);
		bird.transform.position = new Vector3 (-6f, -2f, 0f);
		bird.GetComponent<PositionScript> ().initialPosition = bird.transform.position;
		bird.GetComponent<PositionScript> ().initialVelocity = new Vector2 (velocityX[Random.Range (0, velocityX.Length)], 0f);
		incomingObjects.Add(bird);

		soundwave.SetActive(false); 
		soundwave.transform.position = new Vector3(7f,4,0);
		soundwave.GetComponent<PositionScript>().initialPosition = soundwave.transform.position; 
		soundwave.GetComponent<PositionScript>().initialVelocity = new Vector2(-velocityX[Random.Range (0, velocityX.Length)], 0f); 
		soundwave.GetComponent<SpriteRenderer>().flipX = true;  
		incomingObjects.Add(soundwave); 

		bird = Instantiate(Resources.Load("Prefabs/GrayBird")) as GameObject;
		DontDestroyOnLoad(bird);
		bird.SetActive(false);
		bird.transform.position = new Vector3 (6f, 3f, 0f);
		bird.GetComponent<PositionScript> ().initialPosition = bird.transform.position;
		bird.GetComponent<PositionScript> ().initialVelocity = new Vector2 (-velocityX[Random.Range (0, velocityX.Length)], 0f);
		bird.GetComponent<SpriteRenderer> ().flipX = true;
		incomingObjects.Add(bird);

		soundwave = Instantiate(Resources.Load("Prefabs/SoundWave")) as GameObject;
		DontDestroyOnLoad(soundwave);
		soundwave.SetActive(false); 
		soundwave.transform.position = new Vector3(7f,2,0);
		soundwave.GetComponent<PositionScript>().initialPosition = soundwave.transform.position; 
		soundwave.GetComponent<PositionScript>().initialVelocity = new Vector2(-velocityX[Random.Range (0, velocityX.Length)], 0f); 
		soundwave.GetComponent<SpriteRenderer>().flipX = true;  
		incomingObjects.Add(soundwave);

		bird = Instantiate(Resources.Load("Prefabs/YellowBird")) as GameObject;
		DontDestroyOnLoad(bird);
		bird.SetActive(false);
		bird.transform.position = new Vector3 (6f, -1f, 0f);
		bird.GetComponent<PositionScript> ().initialPosition = bird.transform.position;
		bird.GetComponent<PositionScript> ().initialVelocity = new Vector2 (-velocityX[Random.Range (0, velocityX.Length)], 0f);
		bird.GetComponent<SpriteRenderer> ().flipX = true;
		incomingObjects.Add(bird);

		soundwave = Instantiate(Resources.Load("Prefabs/SoundWave")) as GameObject;
		DontDestroyOnLoad(soundwave);
		soundwave.SetActive(false); 
		soundwave.transform.position = new Vector3(-7f,2.5f,0);
		soundwave.GetComponent<PositionScript>().initialPosition = soundwave.transform.position; 
		soundwave.GetComponent<PositionScript>().initialVelocity = new Vector2(velocityX[Random.Range (0, velocityX.Length)], 0f); 
		soundwave.GetComponent<SpriteRenderer>().flipX = true;  
		incomingObjects.Add(soundwave);

		bird = Instantiate(Resources.Load("Prefabs/YellowBird")) as GameObject;
		DontDestroyOnLoad(bird);
		bird.SetActive(false);
		bird.transform.position = new Vector3 (-6f, 1.5f, 0f);
		bird.GetComponent<PositionScript> ().initialPosition = bird.transform.position;
		bird.GetComponent<PositionScript> ().initialVelocity = new Vector2 (velocityX[Random.Range (0, velocityX.Length)], 0f);
		incomingObjects.Add(bird);

		bird = Instantiate(Resources.Load("Prefabs/RedBird")) as GameObject;
		DontDestroyOnLoad(bird);
		bird.SetActive(false);
		bird.transform.position = new Vector3 (6f, 0f, 0f);
		bird.GetComponent<PositionScript> ().initialPosition = bird.transform.position;
		bird.GetComponent<PositionScript> ().initialVelocity = new Vector2 (-velocityX[Random.Range (0, velocityX.Length)], 0f);
		bird.GetComponent<SpriteRenderer> ().flipX = true;
		incomingObjects.Add(bird);

		soundwave = Instantiate(Resources.Load("Prefabs/SoundWave")) as GameObject;
		DontDestroyOnLoad(soundwave);
		soundwave.SetActive(false); 
		soundwave.transform.position = new Vector3(-7f,2.5f,0f);
		soundwave.GetComponent<PositionScript>().initialPosition = soundwave.transform.position; 
		soundwave.GetComponent<PositionScript>().initialVelocity = new Vector2(velocityX[Random.Range (0, velocityX.Length)], 0f); 
		soundwave.GetComponent<SpriteRenderer>().flipX = true;  
		incomingObjects.Add(soundwave); 

		bird = Instantiate(Resources.Load("Prefabs/RedBird")) as GameObject;
		DontDestroyOnLoad(bird);
		bird.SetActive(false);
		bird.transform.position = new Vector3 (-6f, 2f, 0f);
		bird.GetComponent<PositionScript> ().initialPosition = bird.transform.position;
		bird.GetComponent<PositionScript> ().initialVelocity = new Vector2 (velocityX[Random.Range (0, velocityX.Length)], 0f);
		incomingObjects.Add(bird);

		bird = Instantiate(Resources.Load("Prefabs/BlueBird")) as GameObject;
		DontDestroyOnLoad(bird);
		bird.SetActive(false);
		bird.transform.position = new Vector3 (6f, 4f, 0f);
		bird.GetComponent<PositionScript> ().initialPosition = bird.transform.position;
		bird.GetComponent<PositionScript> ().initialVelocity = new Vector2 (-velocityX[Random.Range (0, velocityX.Length)], 0f);
		bird.GetComponent<SpriteRenderer> ().flipX = true;
		incomingObjects.Add(bird);

		bird = Instantiate(Resources.Load("Prefabs/BlueBird")) as GameObject;
		DontDestroyOnLoad(bird);
		bird.SetActive(false);
		bird.transform.position = new Vector3 (-6f, 1f, 0f);
		bird.GetComponent<PositionScript> ().initialPosition = bird.transform.position;
		bird.GetComponent<PositionScript> ().initialVelocity = new Vector2 (velocityX[Random.Range (0, velocityX.Length)], 0f);
		incomingObjects.Add(bird);

		soundwave = Instantiate(Resources.Load("Prefabs/SoundWave")) as GameObject;
		DontDestroyOnLoad(soundwave);
		soundwave.SetActive(false); 
		soundwave.transform.position = new Vector3(7f,3,0);
		soundwave.GetComponent<PositionScript>().initialPosition = soundwave.transform.position; 
		soundwave.GetComponent<PositionScript>().initialVelocity = new Vector2(-velocityX[Random.Range (0, velocityX.Length)], 0f); 
		soundwave.GetComponent<SpriteRenderer>().flipX = true;  
		incomingObjects.Add(soundwave); 
	}

	void Update () 
	{
		if (gameStarted) 
		{
			totalTime += Time.deltaTime;
			timePassed += Time.deltaTime;
			if (timePassed >= timeDelay) 
			{
				bool found = false;
				int tries = 0;
				while (!found && tries < 6) 
				{
					GameObject obj = incomingObjects [Random.Range (0, incomingObjects.Count)];
					if (!obj.activeSelf) 
					{
						obj.SetActive (true);
						found = true;
					}
					tries += 1;
				}
				timePassed = 0;
			}
			if (totalTime > 15) 
			{
				timeDelay = 0.5f;
			} 
			else if (totalTime > 5) 
			{
				timeDelay = 1;
			}
		}
	}

	public void Reset()
	{
		gameStarted = false;
		timeDelay = 1.5f;
		timePassed = 0f;
		totalTime = 0f; 
		if (incomingObjects != null) 
		{
			foreach (GameObject obj in incomingObjects) 
			{
				if (null != obj) 
				{
					obj.SetActive (false);
				}
			}
		}
	}
}
